# My awesome project



Run the script using this bash command

python email_reader_and_writer.py --CSVin < name of csv in > --CSVout < file location for CSV out > 


Steps for contributing:

    checkout master - git checkout master
    pull - git pull origin master
    make new feature branch - git checkout -b {name of feature}
    make your changes
    add your changes - git add {name of file}
    add a commit - git commit -m '{message}'
    push your feature branch - git push origin {name of feature}
    make merge request in the web application
    assign the merge request to a maintainer
    notify the maintainer
    get your stuff merged in
    go pull to your local master branch again - git checkout master --> git pull origin master
    delete your local feature branch - git branch -d {name of feature}