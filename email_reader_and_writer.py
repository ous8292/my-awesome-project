


import csv
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("--CSVin", help="This is csv in")
parser.add_argument("--CSVout", help="This is csv out")
args = parser.parse_args()
CSVin = args.CSVin
CSVout = args.CSVout


email_listy = []


with open(CSVin, 'r') as csv_file:
	csv_reader = csv.reader(csv_file)

	for row in csv_reader:
		name = row[0]
		location = row[1]

		email= 'Hi ' + name + ', Currently Clarus does not serve the ' + location + ' area. For new areas, we work on a bulk basis with HOAs. Do you reside within an HOA that I could speak with about providing services? If so, please send me their contact information and we will look into this area. Thanks!'
		email_listy.append(email)


with open(CSVout, 'w') as new_list:
	for email in email_listy:
		new_list.write(email + "\n")









 